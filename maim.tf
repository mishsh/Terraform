provider "aws" {
  region     = "eu-west-1"
  access_key = var.access_key
  secret_key = var.mysecret_key
}

resource "aws_instance" "ec2_example" { #master 
  ami                    = var.ami
  instance_type          = "t2.micro"
  key_name               = "newonw"
  vpc_security_group_ids = [aws_security_group.main.id]
  tags = {
    Name = "master"
  }

  provisioner "file" {
    source      = "/home/mish/Documents/terraform/terraform-jenkins/jenkins_install.sh"
    destination = "~/jenkins_install.sh"
  }

    provisioner "file" {
    source      = "~/.ssh/newonw.pem"
    destination = "~/.ssh/newonw.pem"
  }

  provisioner "remote-exec" {
    inline = ["chmod 400 ~/.ssh/newonw.pem"]
  }

  provisioner "remote-exec" {
    inline = [" bash jenkins_install.sh"]
  }

  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file("~/.ssh/newonw.pem")
  }
}
resource "aws_instance" "ec2_example2" { #slave
  ami                    = var.ami
  instance_type          = "t2.micro"
  key_name               = "newonw"
  vpc_security_group_ids = [aws_security_group.main.id]
  tags = {
    Name = "slave"
  }
  provisioner "file" {
    source      = "/home/mish/Documents/terraform/terraform-jenkins/useradd.sh"
    destination = "~/useradd.sh"
  }

  provisioner "file" {
    source      = "/home/mish/.ssh/newonw.pem"
    destination = "~/.ssh/newonw.pem"
  }

 provisioner "remote-exec" {
   inline = ["chmod 400 ~/.ssh/newonw.pem",
             "sh ~/useradd.sh",]   
}

  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file("~/.ssh/newonw.pem")
  }
}

resource "aws_security_group" "main" {
  name        = "Jenkins_sg"
  description = "Allow Jenkins Traffic"
  vpc_id      = var.vpcc

  ingress {
    description = "Allow from Personal CIDR block"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow SSH from Personal CIDR block"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
